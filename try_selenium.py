from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import pdb

caps = DesiredCapabilities().FIREFOX
caps["pageLoadStrategy"] = "normal"
driver = webdriver.Firefox(capabilities=caps)
# driver = webdriver.Firefox()

def fetch_page(url):
	driver.get(url)
	return driver

if __name__ == "__main__":
	# fetch_page("https://seekingalpha.com/article/4071198-eog-resources-eog-q1-2017-results-earnings-call-transcript")
	# content = fetch_page("https://seekingalpha.com/symbol/EOG/earnings/more_transcripts?page=1")
	content = fetch_page("https://seekingalpha.com/article/4170084-eog-resources-eog-q1-2018-results-earnings-call-transcript")
	pdb.set_trace()
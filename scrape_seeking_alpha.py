import requests
import platform
from bs4 import BeautifulSoup
import json
import re
import pdb
import time
import pymongo
from datetime import datetime
from datetime import timedelta
from dateutil.parser import parse as dateparser
import logging
from itertools import cycle
# import scrape_proxy
# import try_selenium

STOCKS_INSCOPE = ["OXY", "EOG", "APC", "APA", "COP", "PXD"]
START_DATE = "2018-07-01"

headers_dict = {
    "authority": "seekingalpha.com",
    "method": "GET",
    "scheme": "https",
    "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
    "accept-encoding": "gzip, deflate, br",
    "accept-language": "en-US,en;q=0.9",
    "cache-control": "max-age=0",
    "if-none-match": "ccc54ad410115731b07e82f6cdaee299",
    "upgrade-insecure-requests": "1",
    "user-agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.96 Safari/537.36"
}

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["seeking_alpha"]
stocks_coll = mydb["stocks"]
earnings_articles_coll = mydb["earnings_articles"]
news_coll = mydb["news"]

def check_word_in_list(word, mylist):
    for elem in mylist:
        if word.lower() in elem.lower() or elem.lower() in word.lower():
            return True

def check_word_in_object(word, myobject):
    if type(myobject) == list:
        return check_word_in_list(word, myobject)
    elif type(myobject) == dict:
        for k, v in myobject.iteritems():
            output = check_word_in_list(word, v)
            if output:
                return True



def fetch_url(url):
    resp = None
    with requests.Session() as sess:
        print "fetching", url
        resp = sess.get(url, headers=headers_dict)
        if resp.status_code != 200:
            pdb.set_trace()
            print "***PAGE NOT DOWNLOADED***"
            resp = fetch_url(url)

    return resp

def get_stock_prices(stock_symbol):
    today = datetime.now()
    today = datetime(today.year, today.month, today.day)

    stock_end_date = stocks_coll.find_one({"Symbol": stock_symbol}, {"End_Date": 1})
    if stock_end_date:
        stock_end_date = stock_end_date["End_Date"]

        if today <= stock_end_date:
            return
        else:
            start_date = stock_end_date + timedelta(days=1)
            start_date = start_date.strftime("%Y-%m-%d")
    else:
        start_date = START_DATE

    resp = fetch_url("https://static.seekingalpha.com/cdn/xignite/globalhistorical/v3/xGlobalHistorical.json/GetGlobalHistoricalQuotesRange?IdentifierType=Symbol&Identifier=" + stock_symbol + "&IdentifierAsOfDate=&AdjustmentMethod=&StartDate=" + start_date + "&EndDate=")

    data = resp.json()
    stock_data = data["Security"]
    stock_data["End_Date"] = today


    historical_quotes_data = data["HistoricalQuotes"]

    if not historical_quotes_data:
        return

    for historic_quote in historical_quotes_data:
        historic_quote["Date"] = datetime.strptime(historic_quote["Date"], '%Y-%m-%d')
        historic_quote["PreviousCloseDate"] = datetime.strptime(historic_quote["PreviousCloseDate"], '%Y-%m-%d')


    stock_data["HistoricalQuotes"] = historical_quotes_data

    if stocks_coll.find_one({"Symbol": stock_symbol}):
        stocks_coll.update({"Symbol": stock_symbol}, {"$addToSet": {"HistoricalQuotes": {"$each": stock_data["HistoricalQuotes"]}}})
    else:
        stocks_coll.insert(stock_data)



def get_fin_numbers(stock_symbol):
    resp = fetch_url("https://seekingalpha.com/symbol/" + stock_symbol + "/overview")

    soup = BeautifulSoup(resp.content, features="html.parser")
    scripts = soup.findAll("script")

    for script in scripts:
        script_text = script.text
        if "nocf" in script_text:
            nocf = None
            search = re.search(r'\"nocf\"\s*\:(.*?)\,', script_text)
            if search:
                nocf = float(search.group(1))
                stocks_coll.update({"Symbol": stock_symbol}, {"$set": {"NetCashFlow": nocf}})

            market_cap = None
            search = re.search(r'\"market_cap\"\s*\:(.*?)\,', script_text)
            if search:
                market_cap = float(search.group(1))
                stocks_coll.update({"Symbol": stock_symbol}, {"$set": {"MarketCap": market_cap}})
            break


def get_transcripts(stock_symbol):
    for page_num in range(1, 20):
        transcript_page_url = "https://seekingalpha.com/symbol/" + stock_symbol + "/earnings/more_transcripts?page=" + str(page_num)
        print "transcript_page_url", transcript_page_url
        resp = fetch_url(transcript_page_url)

        soup = BeautifulSoup(resp_json["html"], features="html.parser")

        articles = soup.select(".symbol_article")

        for article in articles:
            if "transcript" not in article.text.lower():
                continue

            article_name = article.text
            article_url = "https://seekingalpha.com" + article.a["href"]
            article_id = re.search(r'^\/article\/(\d.+?)\-', article.a["href"]).group(1)

            print "article_url", article_url

            resp_article = fetch_url(article_url)

            article_soup = BeautifulSoup(resp_article, features="html.parser")
            article_body = article_soup.select("#a-body")
            if not article_body:
                continue

            article_p = article_body[0].select(".p")

            if article_p:
                datetime_text = article_p[0].text.split()[-6:]
                transcript_date = dateparser(' '.join(datetime_text))
                del article_p[0]
            else:
                continue

            headings_content = {}
            events = []
            flush_list = []
            index = 1
            last_strong_text = None
            next_p_statement = False
            for i, p in enumerate(article_p):
                strong_p = p.select("strong")
                if strong_p:
                    last_strong_text = strong_p[0].text
                    if check_word_in_object(last_strong_text, headings_content):
                        next_p_statement = True
                        continue
                    headings_content[last_strong_text] = []
                else:
                    if next_p_statement or (i > 25 and len(p.text.split()) > 10):
                        events.append({
                                "speaker": last_strong_text,
                                "statement": p.text,
                                "index": index
                            })
                        index += 1
                    else:
                        if last_strong_text not in headings_content:
                            events.append({
                                "speaker": last_strong_text,
                                "statement": p.text,
                                "index": index
                            })
                            index += 1
                        else:
                            headings_content[last_strong_text].append(p.text)
                    next_p_statement = False

            headings_content = dict((k, v) for k, v in headings_content.iteritems() if v)

            print "inserting transcript"
            earnings_articles_coll.update({"ArticleId": article_id}, 
                {
                    "ArticleId": article_id,
                    "ArticleName": article_name,
                    "ArticleURL": article_url,
                    "headings_content": headings_content,
                    "events": events,
                    "Symbol": stock_symbol
                }, upsert=True)


def get_news(stock_symbol):
    print "scraping news for", stock_symbol
    for page_num in range(1, 5):
        news_page_url = "https://seekingalpha.com/symbol/" + stock_symbol + "/news/more_news_all?page=" + str(page_num)

        # pdb.set_trace()
        resp = fetch_url(news_page_url)
        if not resp:
            continue
        resp_json = resp.json()
        
        if resp_json["no_more"]:
            break

        soup = BeautifulSoup(resp_json["html"], features="html.parser")
        all_news = soup.select(".symbol_item")

        for news in all_news:
            news_date = dateparser(news.select(".date")[0].text)
            news_title = news.select(".market_current_title")[0].text
            news_url = "https://seekingalpha.com" + news.select(".market_current_title")[0]["href"]
            news_id = re.search(r'\/news\/(\d.+?)\-', news_url).group(1)

            news_summary_soup = news.select(".general_summary")[0].select("li")
            news_summaries = [i.text for i in news_summary_soup]

            print "inserting news"
            insertion_status = news_coll.update({"NewsId": news_id}, 
                {
                    "NewsId": news_id,
                    "NewsTitle": news_title,
                    "NewsURL": news_url,
                    "NewsDate": news_date,
                    "NewsSummaries": news_summaries,
                    "Symbol": stock_symbol
                }, upsert=True)
            




if __name__ == "__main__":
    
    # for stock_symbol in STOCKS_INSCOPE:
        # get_stock_prices(stock_symbol)
        # get_fin_numbers(stock_symbol)
        # get_transcripts(stock_symbol)

    for stock_symbol in STOCKS_INSCOPE:
        get_news(stock_symbol)
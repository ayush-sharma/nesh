from flask import Flask, request, jsonify
import json
import pymongo
from bson.json_util import dumps as ju_dumps
from flask_cors import CORS
from datetime import datetime
import operator
import pdb

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["seeking_alpha"]
stocks_coll = mydb["stocks"]
earnings_articles_coll = mydb["earnings_articles"]
news_coll = mydb["news"]

BID_PRICE = 10

app = Flask(__name__)
CORS(app)


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

@app.route('/get_transcripts', methods=['GET', 'POST'])
def get_transcripts():
    # print "sample entered"
    # import pdb;pdb.set_trace()

    sample_input = request.args
    print "sample_input", sample_input

    transcripts = []
    earnings = earnings_articles_coll.find(sample_input, {"_id": 0})
    speaker_statement = {}
    for transcript in earnings:
        for event in transcript["events"]:
            speaker_statement.setdefault(event["speaker"], 0)
            speaker_statement[event["speaker"]] += len(event["statement"])
        del transcript["events"]
        sorted_speakers = sorted(speaker_statement.items(), key=operator.itemgetter(1), reverse=True)
        transcript["longest_speaker"] = sorted_speakers[0][0]
        transcripts.append(transcript)

    return jsonify({"transcripts": transcripts})


@app.route('/get_stock_prices', methods=['GET', 'POST'])
def get_stock_prices():
    # print "sample entered"
    # import pdb;pdb.set_trace()

    sample_input = request.args
    print "sample_input", sample_input

    stock_data = stocks_coll.find_one(sample_input, {"_id": 0})
    if stock_data:
        for d in stock_data["HistoricalQuotes"]:
            d["Date"] = d["Date"].isoformat()

    return jsonify({"stock_data": stock_data})

@app.route('/get_news', methods=['GET', 'POST'])
def get_news():
    # print "sample entered"
    # import pdb;pdb.set_trace()

    sample_input = request.args
    print "sample_input", sample_input

    news_data = news_coll.find(sample_input, {"_id": 0})

    return jsonify({"news": [news for news in news_data]})


@app.route('/news_search', methods=['GET', 'POST'])
def news_search():
    sample_input = request.args
    print "sample_input", sample_input

    news_data = news_coll.find({"Symbol": sample_input["Symbol"], "$text": {"$search": sample_input["text"]}}, {"score": {"$meta": "textScore"}, "_id": 0})
    news_data = news_data.sort([('score', {'$meta': 'textScore'})])

    return jsonify({"news": [news for news in news_data]})


@app.route('/transcripts_search', methods=['GET', 'POST'])
def transcripts_search():
    sample_input = request.args
    print "sample_input", sample_input

    transcripts_data = earnings_articles_coll.find({"Symbol": sample_input["Symbol"], "$text": {"$search": sample_input["text"]}}, {"score": {"$meta": "textScore"}, "events": 0, "_id": 0})
    transcripts_data = transcripts_data.sort([('score', {'$meta': 'textScore'})])

    return jsonify({"transcripts": [transcript for transcript in transcripts_data]})



if __name__ == '__main__':
    # app.run(port=5000)
    app.run(port=5000, debug=True)

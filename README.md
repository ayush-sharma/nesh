# Scraping Seeking Alpha #
A web scraper project to scrape information from a publicly available website to present insights about an Oil and Gas company.

### Video Link demonstrating the project workflow and dashboard ###
- https://drive.google.com/file/d/1UWG2-BHUB2lLGH0NnOIPWGXcWDJHwWvh/view?usp=sharing
- If you are are not able to open the video using a video software, please drag and drop the downloaded video in any web browser's new tab.

### API links to gather required data ###

##### Get all historic stock prices of a particular Symbol. #####
- pass Symbol and Start_Date
- https://static.seekingalpha.com/cdn/xignite/globalhistorical/v3/xGlobalHistorical.json/GetGlobalHistoricalQuotesRange?IdentifierType=Symbol&Identifier=EOG&IdentifierAsOfDate=&AdjustmentMethod=&StartDate=07/01/2018&EndDate=

##### Get all financial numbers of a particular Symbol #####
- pass Symbol
- https://seekingalpha.com/symbol/EOG/overview

##### Get all the Earnings Analysis articles of a particular Symbol #####
- pass Symbol and iterate through all the page numbers until you get a blank text in html key.
- https://seekingalpha.com/symbol/EOG/earnings/more_transcripts?page=1

##### Get all the news articles of a particular Symbol #####
- pass Symbol and iterate through all the page numbers until you find true value for no_more key.
- https://seekingalpha.com/symbol/EOG/news/more_news_all?page=1

### How to Run ? ###
1. To scrape the seekingalpha website, run *scrape_seeking_alpha.py*
    - It scrapes through historic prices, finnancial numbers, earnings articles and news articles for all the Symbols in scope.
    - The fetched data is stored in a mongoDB named seeking_alpha with the following collections:
        - stocks
        - earnings_articles
        - news
2. Once the scraper fetches the desired data, *run api.py*
    - It serves as a host to the queries from the dashboard.
    - To filter news articles and transcripts, the query parameters are used directly to find the documents in the mongoDB.
3. Now that you have your mongoDB and flask api host running, you can open the *dashboard.html* file (preferably in Google Chrome)
    - Search for any in-scope Symbol in the search bar on top.
    - When pressed enter, the dashboard is filled with the Stock prices trend, important finnancial numbers, transcripts of earnings call & details about it and news articles about the entered Stock Symbol.
    - The transcripts and news articles are collapsible and the user can view any detail of each by clicking on the title.